﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public class Lugar : Entity
    {
        public int Capacidad { get; set; }
        public string Calle { get; set; }
        public string Numero { get; set; }
        public string Depto { get; set; }
        public string Piso { get; set; }
        public string Localidad { get; set; }
        public string Provincia { get; set; }
        public string Pais { get; set; }

        private Lugar()
        {
            
        }

        public Lugar(int capacidad,string calle, string numero, string depto, string piso,
            string localidad, string provincia, string pais) : this()
        {
            if (capacidad > 0)
            {
                this.Capacidad = capacidad;
                this.Calle = calle;
                this.Numero = numero;
                this.Depto = depto;
                this.Piso = piso;
                this.Localidad = localidad;
                this.Provincia = provincia;
                this.Pais = pais;
            }
        }
    }
}
