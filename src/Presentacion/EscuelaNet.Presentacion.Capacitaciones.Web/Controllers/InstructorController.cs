﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EscuelaNet.Presentacion.Capacitaciones.Web.Infraestructura;
using EscuelaNet.Presentacion.Capacitaciones.Web.Models;

namespace EscuelaNet.Presentacion.Capacitaciones.Web.Controllers
{
    public class InstructorController : Controller
    {
        // GET: Instructor
        public ActionResult Index()
        {
            var instructores =
                Context.Instancia.Instructores;
            var model = new InstructorIndexModel()
            {
                Nombre = "Primera prueba",
                Instructores = instructores
            };
            return View(model);
        }

        public ActionResult New()
        {
            var model = new NewInstructorModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult New(NewInstructorModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    Context.Instancia.Instructores.Add(new NewInstructorModel()
                    {
                        Apellido = model.Apellido,
                        Nombre = model.Nombre,
                        Dni = model.Dni,
                        FechaNacimiento = model.FechaNacimiento,
                        Temas = model.Temas
                    });
                    TempData["success"] = "Instructor Creado";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);

            }

        }

        public ActionResult Edit(int id)
        {
            var instructor = Context.Instancia.Instructores[id];
            var model = new NewInstructorModel()
            {
                Id = id,
                Apellido = instructor.Apellido,
                Nombre = instructor.Nombre,
                Dni = instructor.Dni,
                FechaNacimiento = instructor.FechaNacimiento,
                Temas = instructor.Temas
            };
            return View(model);

        }
        [HttpPost]
        public ActionResult Edit(NewInstructorModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    Context.Instancia.Instructores[model.Id].Apellido
                        = model.Apellido;
                    Context.Instancia.Instructores[model.Id].Nombre
                        = model.Nombre;
                    Context.Instancia.Instructores[model.Id].Dni
                        = model.Dni;
                    Context.Instancia.Instructores[model.Id].FechaNacimiento
                        = model.FechaNacimiento;
                    Context.Instancia.Instructores[model.Id].Temas
                        = model.Temas;
                   
                    TempData["success"] = "Edicion de Instructor exitosa.¡Si hay dicha!";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);

            }

        }

        public ActionResult Delete(int id)
        {
            var instructor = Context.Instancia.Instructores[id];
            var model = new NewInstructorModel()
            {
                Id = id,
                Apellido = instructor.Apellido,
                Nombre = instructor.Nombre,
                Dni = instructor.Dni,
                FechaNacimiento = instructor.FechaNacimiento,
                Temas = instructor.Temas
            };
            return View(model);

        }
        [HttpPost]
        public ActionResult Delete(NewInstructorModel model)
        {

            try
            {
                Context.Instancia.Instructores.RemoveAt(model.Id);
                TempData["success"] = "Instructor borrado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }



        }
    }
}
