﻿using EscuelaNet.Dominio.Clientes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Clientes.Web.Infraestructura
{
    public sealed class Contexto
    {
        private static Contexto _instancia = new Contexto();

        private Contexto()
        {
            this.Clientes = new List<Cliente>();

            Clientes.Add(new Cliente("Arcor", "arcor@gmail.com", Categoria.Privado));

            var direccion1 = new Direccion("domicilio1", "localidad1", "provincia1", "pais1");
            var unidad1 = new UnidadDeNegocio("Golosinas", "responsable1", "20-1122334455-7", "responsable@email.com", "03814112233", direccion1);
            var direccion2 = new Direccion("domicilio2", "localidad2", "provincia2", "pais2");
            var unidad2 = new UnidadDeNegocio("Alimentos", "responsable2", "20-5544332211-7", "responsable2@email.com", "03814332211", direccion2);
            var solicitud1 = new Solicitud("titulo1", "descripcion1");
            var solicitud2 = new Solicitud("titulo2", "descripcion2");
            var solicitud3 = new Solicitud("titulo3", "descripcion3");
            var solicitud4 = new Solicitud("titulo4", "descripcion4");
            var solicitud5 = new Solicitud("titulo5", "descripcion5");
            var solicitud6 = new Solicitud("titulo6", "descripcion6");
            var solicitud7 = new Solicitud("titulo7", "descripcion7");

            unidad1.AgregarSolicitud(solicitud1);
            unidad1.AgregarSolicitud(solicitud2);
            unidad1.AgregarSolicitud(solicitud3);
            solicitud1.CambiarEstado(EstadoSolicitud.Desarrollo);
            solicitud1.CambiarEstado(EstadoSolicitud.Produccion);
            unidad1.AgregarSolicitud(solicitud4);
            unidad1.AgregarSolicitud(solicitud5);            
            unidad1.AgregarSolicitud(solicitud6);

            unidad2.AgregarSolicitud(solicitud7);
            solicitud7.CambiarEstado(EstadoSolicitud.Desarrollo);
            solicitud7.CambiarEstado(EstadoSolicitud.Produccion);

            Clientes[0].AgregarUnidad(unidad1);
            Clientes[0].AgregarUnidad(unidad2);

            Clientes.Add(new Cliente("Pepsi", "pepsi@gmail.com", Categoria.Privado));

            Clientes.Add(new Cliente("Samsung", "samsung@gmail.com", Categoria.Privado));

        }

        public List<Cliente> Clientes { get; set; }

        public static Contexto Instancia
        {
            get
            {
                return _instancia;
            }
        }
    }
}