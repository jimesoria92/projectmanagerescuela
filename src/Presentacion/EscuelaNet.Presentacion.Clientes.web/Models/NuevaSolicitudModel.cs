﻿using EscuelaNet.Dominio.Clientes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Clientes.Web.Models
{
    public class NuevaSolicitudModel
    {
        public string Titul { get; set; }

        public int IdC { get; set; }

        public int IdU { get; set; }

        public int IdS { get; set; }
     
        public string Titulo { get; set; }

        public string Descripcion { get; set; }

        public EstadoSolicitud Estado { get; set; }

        

    }
}