﻿using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Conocimientos.Web.Infraestructura
{
    public sealed class Contexto
    {
        private static Contexto _instancia = new Contexto();
        public List<Categoria> Categorias { get; set; }

        private Contexto()
        {

            this.Categorias = new List<Categoria>();
            Categorias.Add(new Categoria(1,"Esto es una descripción 1", "Algo"));

            Categorias.Add(new Categoria(2,"Esto es una descripción 2", "Algo 2"));

            Categorias.Add(new Categoria(3,"Esto es una descripción 3", "Algo 3"));

        }

        public static Contexto Instancia
        {
            get
            {
                return _instancia;
            }
        }

    }
}
