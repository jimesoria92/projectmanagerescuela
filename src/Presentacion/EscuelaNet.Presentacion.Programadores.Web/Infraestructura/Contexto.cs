﻿using EscuelaNet.Dominio.Programadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Programadores.Web.Infraestructura
{
    public sealed class Contexto
    {
        private static Contexto _instancia = new Contexto();
        public List<Equipo> Equipos { get; set; }


        private Contexto()
        {

            this.Equipos = new List<Equipo>();
            Equipos.Add(new Equipo("Tiburones", "Argentina", 10));

            Equipos.Add(new Equipo("Comadreja", "España", 12));

            Equipos.Add(new Equipo("Camaleones", "Alemania", -10));

        }

        public static Contexto Instancia
        {
            get
            {
                return _instancia;
            }
        }
    }
}